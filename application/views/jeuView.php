<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title><?php echo $titre; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet prefetch" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link rel="stylesheet prefetch" href="https://fonts.googleapis.com/css?family=Coda">
    <link rel="stylesheet prefetch" href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah|Permanent+Marker" >
    <!-- 
        On utilise la fonction css_url() du fichier application/helpers/assets_helper.php
        créé pour simplifier l'accès aux fichiers assets/css/*.css générés par Compass.
        NOTE : la ligne consacrée à IE est toujours générée par Compass mais ce navigateur n'est plus supporté par Microsoft.
     -->
    <link href="<?php echo css_url('screen'); ?>" media="screen, projection" rel="stylesheet" type="text/css" />
    <link href="<?php echo css_url('print'); ?>" media="print" rel="stylesheet" type="text/css" />
    <!--[if IE]>
        <link href="<?php echo css_url('ie'); ?>" media="screen, projection" rel="stylesheet" type="text/css" />
    <![endif]-->
</head>

<body>

    <i class="container">
        <header>
            <h1><?php echo $titre; ?></h1>
        </header>

        <section class="score-panel">
            <div class="bestScores">
            <h3>Meilleurs scores</h3>
            <?php
            $data = array();
            for ($i=0; $i <= count($resultat)-1; $i++) { 
                $data[] = $resultat[$i]['date'] . " :   " . $resultat[$i]['tries'] . " essais  en " . $resultat[$i]['timeInSeconds'] . " \"";
            }
            echo ul($data);
            ?>
            </div>

            <h3>Votre score actuel</h3>
        	<ul class="stars">
        		<li><div class="fa fa-star"></div></li>
        		<li><div class="fa fa-star"></div></li>
        		<li><div class="fa fa-star"></div></li>
        	</ul>

        	<span class="moves">0</span> Essai(s)

            <div class="timer">
            </div>

            <div class="restart" onclick=startGame()>
        		<div class="fa fa-repeat"></div>
        	</div>
        </section>

        <ul class="grille" id="carte-grille">
            <li class="carte" type="icon-pomme-rouge">
                <i class="icon icon-pomme-rouge"></i>
            </li>
            <li class="carte" type="icon-bananes">
                <i class="icon icon-bananes"></i>
            </li>
            <li class="carte match" type="icon-orange">
                <i class="icon icon-orange"></i>
            </li>
            <li class="carte" type="icon-citron-vert" >
                <i class="icon icon-citron-vert"></i>
            </li>
            <li class="carte" type="icon-grenade">
                <i class="icon icon-grenade"></i>
            </li>
            <li class="carte match" type="icon-abricot-1">
                <i class="icon icon-abricot-1"></i>
            </li>
            <li class="carte" type="icon-citron-jaune">
                <i class="icon icon-citron-jaune"></i>
            </li>
            <li class="carte" type="icon-fraise">
                <i class="icon icon-fraise"></i>
            </li>
            <li class="carte" type="icon-pomme-verte">
                <i class="icon icon-pomme-verte"></i>
            </li>
            <li class="carte" type="icon-abricot-2">
                <i class="icon icon-abricot-2"></i>
            </li>
            <li class="carte" type="icon-raisin">
                <i class="icon icon-raisin"></i>
            </li>
            <li class="carte" type="icon-pasteque">
                <i class="icon icon-pasteque"></i>
            </li>
            <li class="carte open show" type="icon-prune">
                <i class="icon icon-prune"></i>
            </li>
            <li class="carte" type="icon-poire">
                <i class="icon icon-poire"></i>
            </li>
            <li class="carte" type="icon-cerises">
                <i class="icon icon-cerises"></i>
            </li>
            <li class="carte" type="icon-framboise">
                <i class="icon icon-framboise"></i>
            </li>
            <li class="carte" type="icon-mangue">
                <i class="icon icon-mangue"></i>
            </li>
            <li class="carte" type="icon-groseilles">
                <i class="icon icon-groseilles"></i>
            </li>
            <li class="carte" type="icon-pomme-rouge">
                <i class="icon icon-pomme-rouge"></i>
            </li>
            <li class="carte" type="icon-bananes">
                <i class="icon icon-bananes"></i>
            </li>
            <li class="carte match" type="icon-orange">
                <i class="icon icon-orange"></i>
            </li>
            <li class="carte" type="icon-citron-vert" >
                <i class="icon icon-citron-vert"></i>
            </li>
            <li class="carte" type="icon-grenade">
                <i class="icon icon-grenade"></i>
            </li>
            <li class="carte match" type="icon-abricot-1">
                <i class="icon icon-abricot-1"></i>
            </li>
            <li class="carte" type="icon-citron-jaune">
                <i class="icon icon-citron-jaune"></i>
            </li>
            <li class="carte" type="icon-fraise">
                <i class="icon icon-fraise"></i>
            </li>
            <li class="carte" type="icon-pomme-verte">
                <i class="icon icon-pomme-verte"></i>
            </li>
            <li class="carte" type="icon-abricot-2">
                <i class="icon icon-abricot-2"></i>
            </li>
            <li class="carte" type="icon-raisin">
                <i class="icon icon-raisin"></i>
            </li>
            <li class="carte" type="icon-pasteque">
                <i class="icon icon-pasteque"></i>
            </li>
            <li class="carte open show" type="icon-prune">
                <i class="icon icon-prune"></i>
            </li>
            <li class="carte" type="icon-poire">
                <i class="icon icon-poire"></i>
            </li>
            <li class="carte" type="icon-cerises">
                <i class="icon icon-cerises"></i>
            </li>
            <li class="carte" type="icon-framboise">
                <i class="icon icon-framboise"></i>
            </li>
            <li class="carte" type="icon-mangue">
                <i class="icon icon-mangue"></i>
            </li>
        </ul>

        <div id="popup1" class="overlay">
            <div class="popup">
                <h2>Félicitations 🎉</h2>
                <a class="close" href=# >×</a>
                <div class="content-1">
                    Bravo, tu as réussi 🎉🎉
                </div>
                <div class="content-2">
                    <p>Après <span id=finalMove> </span> essais </i>
                    <p>en <span id=totalTime> </span> </i>
                    <p>Classement:  <span id=starRating></span></i>
                </div>
                <button id="play-again"onclick="playAgain()">
                    Nouvelle partie 😄</a>
                </button>
            </div>
        </div>

    </div>

    <!-- 
        On utilise la fonction js_url() du fichier application/helpers/assets_helper.php
        créé pour simplifier l'accès au fichier assets/js/app.js.
     -->
    <script src="<?php echo js_url('app'); ?>"></script>
</body>
</html>
