<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JeuModel extends CI_Model
{
    // Constructeur
    public function __construct()
    {
        parent::__construct(); // obligatoire
    }

    public function readTable($table)
    {
        $this->db->from($this->$table);
        $this->db->order_by("tries", "asc");
        $query = $this->db->get($table, 5);
        return $rs->result_array(); // renvoie un tableau dont chaque case contient un tableau assoc. correspondant à une ligne de $rs 
    }
}
