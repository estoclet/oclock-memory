# TP : Jeu "Memory"

Salut ! Le jeu **Memory** est un jeu trés connu et tout aussi simple : il consiste à trouver des paires de cartes identiques le plus rapidement possible en un minimum de coups.


# Objectifs de ce TP

- Découverte du _design patterns_ le plus connus : _MVC_,
- Découverte d'un _framework PHP_ idéal pour débutants : _CodeIgniter_,
- Découverte des bases de données et plus spécifiquement MySQL,
- Révision des bases en HTML/JS/CSS
- Découverte de la containerisation Docker au travers de ddev.io.


# Pré-requis

- Suivre les instructions d'installation de *[DDEV](https://ddev.readthedocs.io/en/stable/#installation)*,
- Cloner ce dépôt,
- Dans un terminal, placez-vous dans le dossier du projet et tapez `ddev config`, puis `ddev start`,
- Toujours dans le terminal, importez la base de données en tapant `ddev import-db --src=database/database.sql`
- Ouvrez votre navigateur à l'adresse *[https://oclock-tt-jeumemory.ddev.site/](https://oclock-tt-jeumemory.ddev.site/)*
